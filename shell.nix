{ nixpkgs ? import (builtins.fetchGit {
    name = "nixpkgs-release-21.11";
    url = "https://github.com/nixos/nixpkgs.git";
    ref = "refs/heads/release-21.11";
    rev = "1a1ba3cdd57b0337c32851bcac037df5eceab749";
  })
  {}, compiler ? "ghc921", doBenchmark ? false }:
let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, hpack, lib }:
      mkDerivation {
        pname = "skew-tool";
        version = "0.0.0.0";
        src = ./.;
        libraryHaskellDepends = [ base ];
        libraryToolDepends = [ hpack ];
        prePatch = "hpack";
        license = lib.licenses.agpl;
      };

  haskellPackages = pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv