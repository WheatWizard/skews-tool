{-# Language GADTs #-}
{-# Language DataKinds #-}
{-# Language KindSignatures #-}
{-# Language FlexibleContexts #-}
module Matrix
  ( Matrix (..)
  , ($$)
  , (#.)
  , (#^)
  , transpose
  , extend
  , extendr
  , makeId
  , makeSwap
  , order
  )
  where


import Prelude hiding
  ( zipWith
  )


import Control.Applicative
import Data.List hiding
  ( transpose
  , zipWith
  )
import Data.Semialign


import Nat
import Vector


type Matrix (n :: Nat) (m :: Nat) a
  = Vec n (Vec m a)


-- | Apply a matrix to a vector.
($$) ::
  ( Num a
  )
    => Matrix n m a -> Vec m a -> Vec n a
m $$ v =
  fmap (sum . zipWith (*) v) m


-- | Transpose a matrix
transpose ::
  ( Applicative (Vec m)
  )
    => Matrix n m a -> Matrix m n a
transpose V0 =
  pure V0
transpose (x :+ xs) =
  zipWith (:+) x $ transpose xs


-- | Compose two matrices.
--
-- Also called matrix multiplication.
(#.) ::
  ( Applicative (Vec k)
  , Num a
  )
    => Matrix i j a -> Matrix j k a -> Matrix i k a
m1 #. m2 =
  fmap
    ( \ r1 ->
      fmap
        ( \ r2 ->
          sum $ zipWith (*) r1 r2
        )
        (transpose m2)
    )
    m1


-- | Raise a square matrix to a positve power.
(#^) ::
  ( Integral k
  , Num a
  , Inductive n
  )
    => Matrix n n a -> k -> Matrix n n a
m #^ 0 =
  makeId
m #^ 1 =
  m
m #^ k =
  let
    (s, i) =
      divMod k 2
    m' =
      m #^ s
  in
    m' #. m' #. (m #^ i)


-- | Given a square matrix @X@ produce a square matrix which takes vectors of size one larger, preserves the first element and applies @X@ to the rest.
extend ::
  ( Num a
  )
    =>  Matrix m m a -> Matrix ('Next m) ('Next m) a
extend V0 =
  V1 (V1 1)
extend m =
  ( ( 1
    :+ (0 <$ m)
    )
  :+ fmap (0 :+) m
  )


-- | Given a square matrix @X@ produce a square matrix which takes vectors of size one larger, preserves the last element and applies @X@ to the rest.
extendr ::
  ( Num a
  )
    =>  Matrix m m a -> Matrix ('Next m) ('Next m) a
extendr V0 =
  V1 (V1 1)
extendr m =
  endCons (endCons 1 $ 0 <$ m) $ fmap (endCons 0) m


-- | Produces an identity matrix of the given size.
makeId ::
  ( Num a
  , Inductive m
  )
    => Matrix m m a
makeId =
  ind2 extend


makeSwap ::
  ( Num a
  , Inductive n
  )
    => Matrix ('Next ('Next n)) ('Next ('Next n)) a
makeSwap =
  ( ( 0
    :+ 1
    :+ pure 0
    )
  :+( 1
    :+ pure 0
    )
  :+ fmap ((0 :+) . (0 :+)) makeId
  )


-- | Determine the order of a matrix.
--
-- That is a positive integer t such that @m #^ t@ is the identity matrix.
order ::
  ( Num a
  , Eq a
  , Integral k
  , Inductive m
  )
    => Matrix m m a -> k
order m =
  go $ iterate (#. m) m
  where
    go (x : xs)
      | x == makeId
      =
        1
      | otherwise
      =
        1 + go xs
