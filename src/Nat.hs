{-# Language DataKinds #-}
module Nat
  ( Nat (..)
  , One
  , Two
  , Three
  , Four
  , Five
  , Six
  )
  where


data Nat
  = Zero
  | Next Nat


type One
  = 'Next 'Zero


type Two
  = 'Next ('Next 'Zero)


type Three
  = 'Next ('Next ('Next 'Zero))


type Four
  = 'Next ('Next ('Next ('Next 'Zero)))


type Five
  = 'Next ('Next ('Next ('Next ('Next 'Zero))))


type Six
  = 'Next ('Next ('Next ('Next ('Next ('Next 'Zero)))))
