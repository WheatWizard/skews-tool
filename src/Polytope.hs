{-# Language DataKinds #-}
{-# Language KindSignatures #-}
{-# Language TypeApplications #-}
{-# Language ScopedTypeVariables #-}
{-# Language FlexibleContexts #-}
{-# Language InstanceSigs #-}
{-# Language GADTs #-}
{-# Language RankNTypes #-}
module Polytope
  ( Polytope (..)
  -- * Operations
  , dual
  , petriePolygon
  , kHole
  -- * Statistics
  , size
  , sType
  -- * Prebuilt polytopes
  , hypercube
  , orthoplex
  , iSimplex
  )
  where


import Prelude hiding
  ( init
  , tail
  , reverse
  )


import Matrix
import Nat
import Vector
import Vector as Vector


-- | A polytope given in terms of distinguished generators.
--
-- @n@ is the rank of the polytope, @m@ is the dimension of ambient space, and @a@ is the numeric type used.
--
-- In order to be a polytope it must obey the laws.
type Polytope n m a =
  Vec n (Matrix m m a)


-- | Gives the dual of a polytope.
dual ::
  (
  )
    => Polytope n m a -> Polytope n m a
dual =
  Vector.reverse


-- | Gives the Petrie polygon of a polytope.
petriePolygon ::
  ( Num a
  , Inductive m
  )
    => Polytope ('Next n) m a -> Polytope Two m a
petriePolygon (x :+ xs) =
  V2 x $ foldr (#.) makeId xs


-- | Gives the kHole of a polyhedron for a given k.
kHole ::
  ( Num a
  , Integral k
  , Inductive m
  )
    => k -> Polytope Three m a -> Polytope Two m a
kHole k (V3 p0 p1 p2) =
  V2 p0 $ p1 #. ((p2 #. p1) #^ (k-1))


-- | Gets the number of edges / vertices in a polygon.
size ::
  ( Num a
  , Eq a
  , Integral k
  , Inductive m
  )
    => Polytope Two m a -> k
size (V2 p0 p1) =
  order $ p0 #. p1


-- | Calculates the Schläfli type of a polytope.
sType ::
  ( Num a
  , Eq a
  , Integral k
  , Inductive m
  )
    => Polytope ('Next n) m a -> Vec n k
sType (_ :+ V0) =
  V0
sType (p1 :+ p2 :+ ps) =
  order (p1 #. p2) :+ sType (p2 :+ ps)


-- | A helper function which takes a orthoplex and gives the orthoplex of the next rank.
upOrthoplex ::
  ( Num a
  , Inductive n
  )
    => Polytope n n a -> Polytope ('Next n) ('Next n) a
upOrthoplex V0 =
  V1 $ V1 $ V1 $ -1
upOrthoplex m@(_ :+ _) =
  -- makeSwap should work here, but GHC cannot figure out that Inductive ('Next n) implies Inductive n so we need to do this weird thing with init.
  (init $ fmap init makeSwap) :+ fmap extend m


-- | Gives the hypercube of a given dimension.
hypercube ::
  ( Num a
  , Inductive n
  )
    => Polytope n n a
hypercube =
  dual orthoplex


-- | Gives the orthoplex of a given diemsnion.
orthoplex ::
  ( Num a
  , Inductive n
  )
    => Polytope n n a
orthoplex =
  ind3 upOrthoplex


-- | Gives a simplex with integral coordinates.
-- The result uses n+1 dimensional coordinates to acheive this.
iSimplex ::
  ( Num a
  , Inductive n
  )
    => Polytope n ('Next n) a
iSimplex =
  Vector.tail hypercube
