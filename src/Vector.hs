{-# Language GADTs #-}
{-# Language DataKinds #-}
{-# Language KindSignatures #-}
{-# Language PatternSynonyms #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language RankNTypes #-}
module Vector
  ( Vec (..)
  , Inductive (..)
  , pattern V1
  , pattern V2
  , pattern V3
  , vScanr
  , vScanr1
  , vScanl
  , vScanl1
  , endCons
  , tail
  , init
  , reverse
  )
  where


import Prelude hiding
  ( zipWith
  , tail
  , init
  , reverse
  )


import Control.Applicative
import Data.Foldable
  as Foldable
import Data.Functor.Classes
import Data.Semialign
import Data.These


import Nat


infixr 5 :+


-- | A vector type.
data Vec (n :: Nat) a where
  V0 :: Vec 'Zero a
  (:+) :: a -> Vec n a -> Vec ('Next n) a


-- | Alias for the empty vector type.
type Vec0 =
  Vec Zero


-- | Alias for vectors with one element.
type Vec1 =
  Vec One


-- | Constructor for 'Vec1'.
{-# Complete V1 #-}
pattern V1 ::
  (
  )
    => a -> Vec1 a
pattern V1 x =
  x :+ V0


-- | Alias for vectors with two elements.
type Vec2 =
  Vec Two


-- | Constructor for 'Vec2'.
{-# Complete V2 #-}
pattern V2 ::
  (
  )
    => a -> a -> Vec2 a
pattern V2 x y =
  x :+ V1 y


-- | Alias for vectors with three elements.
type Vec3 =
  Vec Three


-- | Constructor for 'Vec3'.
{-# Complete V3 #-}
pattern V3 ::
  (
  )
    => a -> a -> a -> Vec3 a
pattern V3 x y z =
  x :+ V2 y z


class Inductive (n :: Nat) where
  ind ::
    (forall m. Inductive m => Vec m a -> Vec ('Next m) a) -> Vec n a
  ind2 ::
    (forall m. Inductive m => Vec m (Vec m a) -> Vec ('Next m) (Vec ('Next m) a)) -> Vec n (Vec n a)
  ind3 ::
    (forall m. Inductive m => Vec m (Vec m (Vec m a)) -> Vec ('Next m) (Vec ('Next m) (Vec ('Next m) a))) -> Vec n (Vec n (Vec n a))


instance Inductive 'Zero where
    ind _ =
      V0
    ind2 _ =
      V0
    ind3 _ =
      V0


instance
  ( Inductive n
  )
    => Inductive ('Next n)
  where
    ind f =
      f $ ind f
    ind2 f =
      f $ ind2 f
    ind3 f =
      f $ ind3 f


instance Functor (Vec n)
  where
    fmap _ V0 =
      V0
    fmap func (x :+ xs) =
      func x :+ fmap func xs


instance
  ( Inductive n
  )
   => Applicative (Vec n)
  where
    pure x =
      ind (x :+)
    liftA2 =
      zipWith


instance
  (
  )
    => Semialign (Vec n)
  where
    alignWith func V0 V0 =
      V0
    alignWith func (x :+ xs) (y :+ ys) =
      func (These x y) :+ alignWith func xs ys


instance
  (
  )
    => Zip (Vec n)
  where
    zipWith _ V0 V0 =
      V0
    zipWith func (x :+ xs) (y :+ ys) =
      func x y :+ zipWith func xs ys


instance
  ( Eq a
  )
    => Eq (Vec n a)
  where
    (==) =
      liftEq (==)


instance Eq1 (Vec n)
  where
    liftEq _ V0 V0 =
      True
    liftEq func (x :+ xs) (y :+ ys) =
      func x y && liftEq func xs ys


instance Foldable (Vec n)
  where
    foldr _ accum V0 =
      accum
    foldr func accum (x :+ xs) =
      func x (foldr func accum xs)


-- TODO optimize
instance
  ( Show a
  )
    => Show (Vec n a)
  where
    show =
      show . Foldable.toList


-- | A right scan on vectors which outputs a vector.
vScanr ::
  (
  )
    => (a -> b -> b) -> b -> Vec n a -> Vec (Next n) b
vScanr _ accum V0 =
  V1 accum
vScanr func accum (x :+ xs) =
  case
    vScanr func accum xs
  of
    y :+ ys ->
      func x y :+ y :+ ys


-- | A version of 'vScanr' that has no starting value argument.
vScanr1 ::
  (
  )
    => (a -> a -> a) -> Vec n a -> Vec n a
vScanr1 _ V0 =
  V0
vScanr1 _ (x :+ V0) =
  V1 $ x
vScanr1 func (x :+ x2 :+ xs) =
  case
    vScanr1 func (x2 :+ xs)
  of
    y :+ ys ->
      func x y :+ y :+ ys


-- | A left scan on vectors which outputs a vector.
vScanl ::
  (
  )
    => (b -> a -> b) -> b -> Vec n a -> Vec (Next n) b
vScanl _ accum V0 =
  V1 accum
vScanl func accum (x :+ xs) =
  accum :+ vScanl func (func accum x) xs



-- | A version of 'vScanl' that has no starting value argument.
vScanl1 ::
  (
  )
    => (a -> a -> a) -> Vec n a -> Vec n a
vScanl1 _ V0 =
  V0
vScanl1 func (x :+ xs) =
  vScanl func x xs


-- | Attach an element to the end of a vector.
endCons ::
  (
  )
    => a -> Vec n a -> Vec ('Next n) a
endCons x V0 =
  V1 x
endCons x (y :+ ys) =
  y :+ endCons x ys


-- | Get all but the last element of a vector.
init ::
  (
  )
    => Vec ('Next n) a -> Vec n a
init (x :+ V0) =
  V0
init (x :+ xs@(_ :+ _)) =
  x :+ init xs


-- | Get all but the first element of a vector.
tail ::
  (
  )
    => Vec ('Next n) a -> Vec n a
tail (_ :+ xs) =
  xs


-- TODO try and make this faster.
-- | Reverse a vector.
reverse ::
  (
  )
    => Vec n a -> Vec n a
reverse V0 =
  V0
reverse (x :+ xs) =
  endCons x $ reverse xs
